

function App() {
  return (
    <div className="App">
      <header>Ace Todo List</header>
      <main>Content</main>
      <footer>Footer</footer>
    </div>
  );
}

export default App;
